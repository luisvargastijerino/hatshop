package com.hatshop_api.services

import com.hatshop_api.models.Order
import com.hatshop_api.repositories.OrdersRepository
import com.hatshop_api.repositories.ProductRepository
import jakarta.transaction.Transactional
import org.springframework.stereotype.Service
import java.math.BigDecimal
import kotlin.jvm.optionals.getOrNull

@Service
class OrdersService(
  private val ordersRepository: OrdersRepository,
  private val productRepository: ProductRepository
) {
  @Transactional
  fun save(order: Order): Order {
    val savedOrder =
      if (order.id == null) {
        val newOrder = order.copy(
          totalAmount = BigDecimal.ZERO,
          orderDetails = mutableSetOf()
        )
        ordersRepository.save(newOrder)
      } else {
        val savedOrder = ordersRepository.findById(order.id!!).get()
        val orderDetailsToDel = savedOrder.orderDetails - savedOrder.orderDetails
        savedOrder.orderDetails.removeAll(orderDetailsToDel)
        savedOrder
      }
    val orderDetails = order.orderDetails.associateBy { it.id.productId }
    savedOrder.orderDetails.addAll(order.orderDetails)
    savedOrder.orderDetails.forEach {
      it.id.orderId = savedOrder.id
      productRepository.findById(it.id.productId!!).getOrNull()?.let { p ->
        it.product = p
        it.productName = p.name
        it.unitCost =
          if (p.discountedPrice.compareTo(BigDecimal.ZERO) != 0) p.discountedPrice
          else p.price
      }
      it.order = savedOrder
      it.quantity = orderDetails[it.id.productId]!!.quantity
    }
    val totalBeforeTaxes = savedOrder.orderDetails.sumOf { (it.unitCost ?: BigDecimal.ZERO) * BigDecimal(it.quantity) }
    savedOrder.totalAmount =
      totalBeforeTaxes * (BigDecimal.ONE + (order.tax?.taxPercentage ?: BigDecimal.ZERO) / BigDecimal(100))

    return ordersRepository.save(savedOrder)
  }
}
