package com.hatshop_api.security.repositories

import com.hatshop_api.security.models.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Int> {
  fun findOneByUsername(username: String): User?
}
