package com.hatshop_api.security

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod.GET
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.invoke
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.csrf.CookieCsrfTokenRepository
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler

@Configuration
@EnableWebSecurity
class SecurityConfig {

  companion object {
    @JvmStatic
    private val LOGGER = LoggerFactory.getLogger(SecurityConfig::class.java)
  }

  @Bean
  fun passwordEncoder() = BCryptPasswordEncoder()

  @Bean
  fun filterChain(http: HttpSecurity): SecurityFilterChain {
    LOGGER.debug("configuring HttpSecurity")
    http {
      authorizeRequests {
        listOf("/*", "/assets/**", "/fonts/**").forEach { authorize(it, permitAll) }
        listOf("/api/products/**", "/api/departments/**", "/api/categories/**").forEach {
          authorize(
            GET,
            it,
            permitAll
          )
        }
        listOf("/api/hal/products/**", "/api/hal/departments/**", "/api/hal/categories/**").forEach {
          authorize(
            GET,
            it,
            permitAll
          )
        }
        authorize("/api/session-user", authenticated)
        authorize("/swagger-ui/**", authenticated)
        authorize("/api/csrf", authenticated)
        authorize("/api/**", hasRole("ADMIN"))
//        authorize("/h2-console/**", permitAll)
      }
      httpBasic {}
      formLogin {
        loginProcessingUrl = "/api/login"
      }
      logout {
        logoutUrl = "/api/logout"
      }
      sessionManagement {
        sessionCreationPolicy = SessionCreationPolicy.IF_REQUIRED
      }
      headers {
        frameOptions { disable() }
      }
    }

    return http.build()
  }

}
