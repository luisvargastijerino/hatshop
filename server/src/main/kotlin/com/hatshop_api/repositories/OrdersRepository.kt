package com.hatshop_api.repositories

import com.hatshop_api.models.Order
import com.lv_spring.data.rest.jpa.JpaRepositoryAndSpecificationExecutor


interface OrdersRepository : JpaRepositoryAndSpecificationExecutor<Order, Int>
