package com.hatshop_api.models

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.AttributeOverride
import jakarta.persistence.AttributeOverrides
import jakarta.persistence.Column
import jakarta.persistence.EmbeddedId
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table
import jakarta.validation.constraints.Size
import java.math.BigDecimal

@Entity
@Table(name = "order_details")
class OrderDetail(
  @EmbeddedId
  @AttributeOverrides(
    AttributeOverride(name = "orderId", column = Column(name = "order_id", nullable = false)),
    AttributeOverride(name = "productId", column = Column(name = "product_id", nullable = false))
  )
  var id: OrderDetailId,

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(nullable = false, insertable = false, updatable = false)
  var order: Order?,

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(nullable = false, insertable = false, updatable = false)
  var product: Product?,

  @Size(max = 50)
  @Column(nullable = false, length = 50)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var productName: String?,

  @Column(nullable = false)
  var quantity: Int,

  @Column(nullable = false, precision = 10)
  var unitCost: BigDecimal?,
) {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as OrderDetail

    return id == other.id
  }

  override fun hashCode(): Int {
    return id.hashCode()
  }
}
