package com.hatshop_api.models

import jakarta.persistence.Column
import jakarta.persistence.Embeddable
import java.io.Serializable

@Embeddable
data class OrderDetailId(
  @Column(nullable = false)
  var orderId: Int?,

  @Column(nullable = false)
  var productId: Int?,
) : Serializable {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as OrderDetailId

    if (orderId != other.orderId) return false
    if (productId != other.productId) return false

    return true
  }

  override fun hashCode(): Int {
    var result = orderId ?: 0
    result = 31 * result + (productId ?: 0)
    return result
  }
}
