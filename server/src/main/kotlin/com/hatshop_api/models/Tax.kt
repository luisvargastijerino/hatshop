package com.hatshop_api.models

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import jakarta.persistence.Table
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import java.math.BigDecimal

@Entity
@Table(name = "taxes")
class Tax(
  @NotEmpty
  @Size(max = 100)
  @Column(nullable = false, length = 100)
  var taxType: String,

  @NotNull
  @Column(nullable = false, precision = 10, scale = 2)
  var taxPercentage: BigDecimal,

  @OneToMany(mappedBy = "tax")
  var orders: MutableSet<Order> = HashSet(0),

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Int? = null,
)
