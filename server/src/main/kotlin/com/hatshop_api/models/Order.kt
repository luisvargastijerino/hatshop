package com.hatshop_api.models

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType.LAZY
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import jakarta.persistence.Table
import jakarta.persistence.Temporal
import jakarta.persistence.TemporalType.TIMESTAMP
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import org.springframework.data.annotation.CreatedDate
import java.math.BigDecimal
import java.util.*


// Generated Jun 19, 2015 2:01:18 AM by Hibernate Tools 4.3.1
@Entity
@Table(name = "orders")
data class Order(
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Int?,

  @ManyToOne(fetch = LAZY)
  var customer: Customer,

  @ManyToOne(fetch = LAZY)
  var shipping: Shipping?,

  @ManyToOne(fetch = LAZY)
  var tax: Tax?,

  @Column(nullable = false, precision = 10)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var totalAmount: BigDecimal?,

  @Temporal(TIMESTAMP)
  @CreatedDate
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var createdOn: Date = Date(),

  @Temporal(TIMESTAMP)
  @Column(length = 29)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var shippedOn: Date?,

  @NotNull
  @Column(nullable = false)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var status: Int = 0,

  var comments: String?,

  @Size(max = 50)
  @Column(length = 50)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var authCode: String?,

  @Size(max = 50)
  @Column(length = 50)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  var reference: String?,

  @OneToMany(mappedBy = "order")
  var audits: MutableSet<Audit>?,

  @NotEmpty
  @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "order")
//  @JoinColumn(name = "order_id")
  var orderDetails: MutableSet<OrderDetail>,
)


