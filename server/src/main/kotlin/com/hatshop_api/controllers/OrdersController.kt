package com.hatshop_api.controllers

import com.hatshop_api.services.OrdersService
import com.hatshop_api.models.Order
import com.hatshop_api.repositories.OrdersRepository
import com.lv_spring.data.rest.jpa.AbstractRestController
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("orders")
@Tag(name = "Orders")
class OrdersController(
  repository: OrdersRepository,
  private val ordersService: OrdersService
) : AbstractRestController<Order, Int>(repository) {

  override fun save(entity: Order): Order {
    return ordersService.save(entity)
  }
}
