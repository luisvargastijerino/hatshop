import {Component, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {ProductsService} from "../../services/products.service";
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs";
import {DepartmentsService} from "../../services/departments.service";
import {CategoriesService} from "../../services/categories.service";
import {environment} from "../../../environments/environment";
import {ShoppingCartService} from "../../services/shopping-cart.service";
import {AuthService} from "../../services/auth.service";
import {GlobalsService} from "../../services/globals.service";
import {ShoppingCart, ShoppingCartLine} from "../../models/shopping-cart";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  popularProducts: Product[] = [];
  productImagesDir = environment.productImagesDir;
  // shoppingCart: ShoppingCart = this.shoppingCartSvc.getGuestCart();

  constructor(productsService: ProductsService,
              departmentsService: DepartmentsService,
              categoriesService: CategoriesService,
              public shoppingCartSvc: ShoppingCartService,
              activatedRoute: ActivatedRoute) {

    activatedRoute.params.pipe(
      switchMap(params => {
        if (params.hasOwnProperty('categoryId')) {
          return categoriesService.findProducts(params['categoryId'])
        } else if (params.hasOwnProperty('departmentId')) {
          return departmentsService.findProducts(params['departmentId'])
        } else {
          return productsService.find();
        }
      })
    ).subscribe(products => {
      this.popularProducts = products.content ?? [];
    })
  }

  ngOnInit(): void {
  }
}
