import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ExternalFormTableComponent } from "../../components/external-form-table/external-form-table.component";
import { MatxTableModule } from "matx-table";


@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ExternalFormTableComponent,
    MatxTableModule
  ]
})
export class ProductsModule { }
