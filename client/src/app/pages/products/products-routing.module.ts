import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products.component';

const routes: Routes = [
  { path: '', component: ProductsComponent },
  { path: 'add', loadChildren: () => import('../product-form/product-form.module').then(m => m.ProductFormModule) },
  { path: 'edit/:id', loadChildren: () => import('../product-form/product-form.module').then(m => m.ProductFormModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
