import { Component } from '@angular/core';
import { ProductsService } from "../../services/products.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.scss'
})
export class ProductsComponent {
  productImagesDir = environment.productImagesDir;

  constructor(public service: ProductsService) {
  }
}
