import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import {MatToolbar} from "@angular/material/toolbar";
import {MatxBackButtonModule} from "matx-core";
import {MatCardImage} from "@angular/material/card";
import {MatAnchor, MatButton, MatIconAnchor} from "@angular/material/button";
import {MatIcon} from "@angular/material/icon";
import {MatBadge} from "@angular/material/badge";


@NgModule({
  declarations: [
    ProductComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    MatToolbar,
    MatxBackButtonModule,
    MatCardImage,
    MatAnchor,
    MatButton,
    MatIcon,
    MatIconAnchor,
    MatBadge
  ]
})
export class ProductModule { }
