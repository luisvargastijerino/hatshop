import {Component} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {ActivatedRoute} from "@angular/router";
import {Product} from "../../models/product";
import {environment} from "../../../environments/environment";
import {ShoppingCartService} from "../../services/shopping-cart.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss'
})
export class ProductComponent {

  product: Product = new Product();
  productImagesDir = environment.productImagesDir;
  shoppingCart = this.shoppingCartSvc.getGuestCart();


  constructor(
    private svc: ProductsService,
    private route: ActivatedRoute,
    public shoppingCartSvc: ShoppingCartService,
  ) {
    const id = route.snapshot.params['productId'];
    if (id) {
      svc.findById(id).subscribe(value => this.product = value);
    }
  }
}
