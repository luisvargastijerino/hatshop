import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatToolbarModule } from "@angular/material/toolbar";
import { NgxPermissionsModule } from "ngx-permissions";
import { RouterTestingModule } from "@angular/router/testing";
import { MatxErrorsModule, MatxMenuButtonModule } from "matx-core";
import { FormsModule } from "@angular/forms";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        HttpClientTestingModule,
        MatToolbarModule,
        NgxPermissionsModule.forRoot(),
        RouterTestingModule,
        MatxMenuButtonModule,
        FormsModule,
        MatxErrorsModule,
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
