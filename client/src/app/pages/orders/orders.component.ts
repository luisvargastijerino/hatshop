import { Component } from '@angular/core';
import { OrdersService } from "../../services/orders.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrl: './orders.component.scss'
})
export class OrdersComponent {
  constructor(public service: OrdersService) {
  }
}
