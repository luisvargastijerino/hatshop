import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders.component';
import { ExternalFormTableComponent } from "../../components/external-form-table/external-form-table.component";
import { MatxTableModule } from "matx-table";


@NgModule({
  declarations: [
    OrdersComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    ExternalFormTableComponent,
    MatxTableModule
  ]
})
export class OrdersModule { }
