import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';

const routes: Routes = [
  { path: '', component: OrdersComponent },
  { path: 'add', loadChildren: () => import('../order-form/order-form.module').then(m => m.OrderFormModule) },
  { path: 'edit/:id', loadChildren: () => import('../order-form/order-form.module').then(m => m.OrderFormModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
