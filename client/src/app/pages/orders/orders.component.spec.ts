import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersComponent } from './orders.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ExternalFormTableComponent } from "../../components/external-form-table/external-form-table.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MatxTableModule } from "matx-table";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

describe('OrdersComponent', () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrdersComponent],
      imports: [
        HttpClientTestingModule,
        ExternalFormTableComponent,
        RouterTestingModule,
        MatxTableModule,
        NoopAnimationsModule,
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
