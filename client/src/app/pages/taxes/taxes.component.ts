import { Component } from '@angular/core';
import { MatIcon } from "@angular/material/icon";
import { MatxErrorsModule } from "matx-core";
import { MatxTableModule } from "matx-table";
import { ReactiveFormsModule } from "@angular/forms";
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { TaxesService } from "../../services/taxes.service";
import { MatError, MatFormField, MatPrefix } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";

@Component({
  selector: 'app-taxes',
  standalone: true,
  imports: [
    EditableTableComponent,
    MatError,
    MatFormField,
    MatIcon,
    MatInput,
    MatPrefix,
    MatxErrorsModule,
    MatxTableModule,
    ReactiveFormsModule
  ],
  templateUrl: './taxes.component.html',
  styleUrl: './taxes.component.scss'
})
export class TaxesComponent  {
  searchFn = (searchParam: string) => `taxType==*${searchParam}*`

  constructor(public service: TaxesService) {
  }

}
