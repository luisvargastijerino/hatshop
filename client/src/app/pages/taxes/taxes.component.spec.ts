import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesComponent } from './taxes.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ExternalFormTableComponent } from "../../components/external-form-table/external-form-table.component";
import { RouterTestingModule } from "@angular/router/testing";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

describe('TaxesComponent', () => {
  let component: TaxesComponent;
  let fixture: ComponentFixture<TaxesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TaxesComponent,
        HttpClientTestingModule,
        ExternalFormTableComponent,
        RouterTestingModule,
        NoopAnimationsModule,
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(TaxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
