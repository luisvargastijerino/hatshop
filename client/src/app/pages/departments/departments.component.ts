import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatxPromptService } from 'matx-core';
import { Department } from '../../models/department';
import { DepartmentsService } from '../../services/departments.service';
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss']
})
export class DepartmentsComponent {

  constructor(public service: DepartmentsService) {
  }
}
