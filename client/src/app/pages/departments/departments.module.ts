import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { DepartmentsComponent } from './departments.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgObjectPipesModule } from 'ngx-pipes';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatxErrorsModule, MatxInputModule, MatxMenuButtonModule, MatxPromptModule } from 'matx-core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatxTableModule } from "matx-table";
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { MatError, MatFormField } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";


@NgModule({
  declarations: [DepartmentsComponent],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    NgObjectPipesModule,
    MatTableModule,
    MatPaginatorModule,
    MatxMenuButtonModule,
    MatxInputModule,
    MatDialogModule,
    MatxTableModule,
    EditableTableComponent,
    MatError,
    MatFormField,
    MatInput,
    MatxErrorsModule,
    ReactiveFormsModule
  ]
})
export class DepartmentsModule {
}
