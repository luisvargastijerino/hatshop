import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepartmentsComponent } from './departments.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ExternalFormTableComponent } from "../../components/external-form-table/external-form-table.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MatxTableModule } from "matx-table";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatxMenuButtonModule } from "matx-core";
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";

describe('DepartmentsComponent', () => {
  let component: DepartmentsComponent;
  let fixture: ComponentFixture<DepartmentsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsComponent ],
      imports: [
        HttpClientTestingModule,
        ExternalFormTableComponent,
        RouterTestingModule,
        MatxTableModule,
        MatxMenuButtonModule,
        NoopAnimationsModule,
        EditableTableComponent,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
