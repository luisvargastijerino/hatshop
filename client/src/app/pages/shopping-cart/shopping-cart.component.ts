import { Component } from '@angular/core';
import {ShoppingCartService} from "../../services/shopping-cart.service";
import {ShoppingCart, ShoppingCartLine} from "../../models/shopping-cart";
import {GlobalsService} from "../../services/globals.service";
import {Location} from "@angular/common";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrl: './shopping-cart.component.scss'
})
export class ShoppingCartComponent {

  shoppingCart: ShoppingCart = this.shoppingCartSvc.getGuestCart();
  productImagesDir = environment.productImagesDir;
  constructor(
    private shoppingCartSvc: ShoppingCartService,
    globalsSvc: GlobalsService,
    private location: Location,
  ) {
    globalsSvc.currentUser$.subscribe(user => {
      let userId = user?.id.toString();
      if (userId) {
        shoppingCartSvc.findById(userId).subscribe(shoppingCart => {
          this.shoppingCart = shoppingCart
        });
      }
    });
  }

  saveShoppingCartLine(shoppingCartLine: ShoppingCartLine) {
    if (shoppingCartLine.quantity === 0) {
      this.shoppingCart.lines = this.shoppingCart.lines.filter(line => line.product?.id !== shoppingCartLine.product?.id);
    }
    this.shoppingCartSvc.save(this.shoppingCart);
  }

  clearShoppingCart() {
    this.shoppingCart.lines = [];
    this.shoppingCartSvc.save(this.shoppingCart).subscribe(() => {
      this.location.back();
    });
  }
}
