import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';
import { ShoppingCartComponent } from './shopping-cart.component';
import {MatToolbar} from "@angular/material/toolbar";
import {MatxBackButtonModule, MatxInputModule} from "matx-core";
import {MatCard, MatCardContent, MatCardHeader, MatCardImage} from "@angular/material/card";
import {FormsModule} from "@angular/forms";
import {MatAnchor} from "@angular/material/button";
import {MatFormField} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";


@NgModule({
  declarations: [
    ShoppingCartComponent
  ],
  imports: [
    CommonModule,
    ShoppingCartRoutingModule,
    MatToolbar,
    MatxBackButtonModule,
    MatCard,
    MatCardContent,
    MatCardHeader,
    FormsModule,
    MatAnchor,
    MatFormField,
    MatInput,
    MatCardImage
  ]
})
export class ShoppingCartModule { }
