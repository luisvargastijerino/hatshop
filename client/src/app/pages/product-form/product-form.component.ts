import { Component } from '@angular/core';
import { Product } from "../../models/product";
import { CategoriesService } from "../../services/categories.service";
import { ProductsService } from "../../services/products.service";
import { ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Location } from "@angular/common";
import { Observable } from "rxjs";
import { Department } from "../../models/department";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrl: './product-form.component.scss'
})
export class ProductFormComponent {

  operation: string;

  model: Product = new Product();

  categories$ = this.categoriesSvc.findAll({$expand: 'categories'}) as Observable<(Department | any)[]>;

  constructor(private svc: ProductsService,
              public categoriesSvc: CategoriesService,
              private route: ActivatedRoute,
              private location: Location) {
    const id = route.snapshot.params['id'];
    this.operation = id ? 'edit' : 'add';
    if (id) {
      svc.findById(id, {$expand: 'categories'}).subscribe(value => this.model = value);
    }
  }

  save(form: NgForm) {
    if (form.invalid) return;

    const product = {
      ...this.model,
      categories: this.model.categories?.map(c => ({id: c.id}))
    }
    this.svc.save(product).subscribe(() => this.location.back());
  }
}
