import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductFormRoutingModule } from './product-form-routing.module';
import { ProductFormComponent } from './product-form.component';
import {
  MatxBackButtonModule,
  MatxErrorsModule,
  MatxInputModule,
  MatxSelectModule,
  MatxTextareaModule
} from "matx-core";
import { MatToolbar } from "@angular/material/toolbar";
import { FormsModule } from "@angular/forms";
import { MatButton } from "@angular/material/button";


@NgModule({
  declarations: [
    ProductFormComponent
  ],
  imports: [
    CommonModule,
    ProductFormRoutingModule,
    MatxBackButtonModule,
    MatToolbar,
    FormsModule,
    MatxInputModule,
    MatxTextareaModule,
    MatxErrorsModule,
    MatxSelectModule,
    MatButton
  ]
})
export class ProductFormModule { }
