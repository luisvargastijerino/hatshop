import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderFormRoutingModule } from './order-form-routing.module';
import { OrderFormComponent } from './order-form.component';
import { MatToolbar } from "@angular/material/toolbar";
import {
  MatxAutocompleteModule,
  MatxBackButtonModule,
  MatxErrorsModule,
  MatxInputModule,
  MatxSelectModule,
  MatxTextareaModule
} from "matx-core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButton, MatIconButton } from "@angular/material/button";
import { MatxTableModule } from "matx-table";
import { MatIcon } from "@angular/material/icon";
import { MatProgressSpinner } from "@angular/material/progress-spinner";
import { MatTooltip } from "@angular/material/tooltip";
import { MatFormField } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";


@NgModule({
  declarations: [
    OrderFormComponent
  ],
  imports: [
    CommonModule,
    OrderFormRoutingModule,
    MatToolbar,
    MatxBackButtonModule,
    FormsModule,
    MatxInputModule,
    MatxTextareaModule,
    MatxErrorsModule,
    MatxSelectModule,
    MatButton,
    MatxAutocompleteModule,
    MatxTableModule,
    MatIcon,
    MatIconButton,
    MatProgressSpinner,
    MatTooltip,
    ReactiveFormsModule,
    MatFormField,
    MatInput,

  ]
})
export class OrderFormModule { }
