import { Component, ViewChild } from '@angular/core';
import { Order } from "../../models/order";
import { OrdersService } from "../../services/orders.service";
import { ProductsService } from "../../services/products.service";
import { ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Location } from "@angular/common";
import { CustomersService } from "../../services/customers.service";
import { map, tap } from "rxjs/operators";
import { MatxRow, MatxTableComponent } from "matx-table";
import { OrderDetail } from "../../models/order-detail";
import { TaxesService } from "../../services/taxes.service";

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrl: './order-form.component.scss'
})
export class OrderFormComponent {

  operation: string;

  model: Order = {
    totalAmount: 0,
    status: 0,
    orderDetails: []
  };

  filterProducts = (filterVal?: string) =>
    this.productsSvc
      .find({...(filterVal && {search: `name==*${filterVal}*,description==*${filterVal}*`})})
      .pipe(map(productsPage => productsPage.content ?? []));

  products$ = this.filterProducts();

  filterCustomers = (filterVal?: string) =>
    this.customersSvc
      .find({...(filterVal && {search: `firstName==*${filterVal}*,lastName==*${filterVal}*`})})
      .pipe(map(customersPage => customersPage.content ?? []));

  customers$ = this.filterCustomers();

  taxes$ = this.taxesSvc.findAll()
    .pipe(tap(taxes => this.model.tax = taxes[0]));

  @ViewChild(MatxTableComponent)
  table!: MatxTableComponent

  constructor(private svc: OrdersService,
              private productsSvc: ProductsService,
              private customersSvc: CustomersService,
              private taxesSvc: TaxesService,
              private route: ActivatedRoute,
              private location: Location) {
    const id = route.snapshot.params['id'];
    this.operation = id ? 'edit' : 'add';
    if (id) {
      svc.findById(id, {$expand: 'customer,orderDetails.product'})
        .subscribe(value => this.model = value);
    }
  }

  save(form: NgForm) {
    if (form.invalid || !this.table?.rows.length) return;

    const order = {
      id: this.model.id,
      customer: {
        id: this.model.customer?.id
      },
      orderDetails: this.table.rows.map(row => ({
        id: {
          orderId: this.model.id,
          productId: row.item.product.id,
        },
        quantity: row.item.quantity,
      } as OrderDetail)),
      tax: this.model.tax
    }
    this.svc.save(order).subscribe(() => this.location.back());
  }

  saveOrderDetail(table: MatxTableComponent, row: MatxRow) {
    table.submitRow(row)
  }

  deleteSelectedOrderDetails() {

  }

  deleteOrderDetailById(id: number) {
    this.table!.rows = this.table?.rows.filter(row => row.item.id !== id)
  }

  addOrderDetail() {
    this.table?.startAdding({
      quantity: 1,
      unitCost: 0,
    } as OrderDetail)
  }

  getUnitCost(row: MatxRow): number {
    const product = row.form?.value?.['product'] ?? row.item?.product;
    return (product?.['discountedPrice'] || product?.['price']) ?? 0;
  }

  getLinePrince(row: MatxRow): number {
    const quantity = row.form?.value?.['quantity'] ?? row.item.quantity
    return this.getUnitCost(row) * quantity
  }

  getTotalBeforeTaxes() {
    return (this.table?.rows ?? []).reduce(
      (currentValue, row) => currentValue + this.getLinePrince(row),
      0
    ) ?? 0;
  }
}
