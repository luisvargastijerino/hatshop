import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatxPromptService } from 'matx-core';
import { Category } from '../../models/category';
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { CategoriesService } from "../../services/categories.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent {

  constructor(public service: CategoriesService) {
  }
}
