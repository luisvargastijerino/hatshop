import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgObjectPipesModule } from 'ngx-pipes';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatxErrorsModule, MatxInputModule, MatxMenuButtonModule } from 'matx-core';
import { MatxTableModule } from "matx-table";
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { MatError, MatFormField } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";


@NgModule({
  declarations: [CategoriesComponent],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    NgObjectPipesModule,
    MatTableModule,
    MatPaginatorModule,
    MatxMenuButtonModule,
    MatxInputModule,
    MatxTableModule,
    EditableTableComponent,
    MatError,
    MatFormField,
    MatInput,
    MatxErrorsModule,
    ReactiveFormsModule
  ]
})
export class CategoriesModule { }
