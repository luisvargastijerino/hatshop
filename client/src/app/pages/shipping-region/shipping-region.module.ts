import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShippingRegionRoutingModule } from './shipping-region-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { NgObjectPipesModule } from 'ngx-pipes';
import { MatxTableModule } from "matx-table";
import { MatPaginatorModule } from '@angular/material/paginator';
import { ShippingRegionComponent } from './shipping-region.component';
import { MatxErrorsModule, MatxInputModule, MatxMenuButtonModule } from 'matx-core';
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { MatError, MatFormField } from "@angular/material/form-field";
import { MatInput } from "@angular/material/input";


@NgModule({
  declarations: [ShippingRegionComponent],
  imports: [
    CommonModule,
    ShippingRegionRoutingModule,
    MatToolbarModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    NgObjectPipesModule,
    MatxTableModule,
    MatPaginatorModule,
    MatxMenuButtonModule,
    MatxInputModule,
    EditableTableComponent,
    MatError,
    MatFormField,
    MatInput,
    MatxErrorsModule,
    ReactiveFormsModule
  ]
})
export class ShippingRegionModule {
}
