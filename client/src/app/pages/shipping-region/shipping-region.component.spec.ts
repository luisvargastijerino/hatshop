import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShippingRegionComponent } from './shipping-region.component';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NgxPermissionsModule } from "ngx-permissions";
import { EditableTableComponent } from "../../components/editable-table/editable-table.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MatxTableModule } from "matx-table";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";

describe('ShippingRegionComponent', () => {
  let component: ShippingRegionComponent;
  let fixture: ComponentFixture<ShippingRegionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippingRegionComponent ],
      imports: [
        HttpClientTestingModule,
        NgxPermissionsModule.forRoot(),
        EditableTableComponent,
        RouterTestingModule,
        MatxTableModule,
        NoopAnimationsModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
