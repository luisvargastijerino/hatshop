import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShippingRegionComponent } from './shipping-region.component';

const routes: Routes = [
  {path: '', component: ShippingRegionComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShippingRegionRoutingModule {}
