import { Component } from '@angular/core';
import { ShippingRegionsService } from '../../services/shipping-regions.service';

@Component({
  selector: 'app-shipping-region',
  templateUrl: './shipping-region.component.html',
  styleUrls: ['./shipping-region.component.scss']
})
export class ShippingRegionComponent {

  constructor(public service: ShippingRegionsService) {

  }
}
