import { finalize } from "rxjs";
import { MatxErrorsModule, MatxInputModule, MatxMenuButtonModule } from "matx-core";
import { MatPaginator } from "@angular/material/paginator";
import { MatxRow, MatxTableComponent, MatxTableModule } from "matx-table";
import { FormGroup, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { merge as _merge } from "lodash-es";
import { Component } from "@angular/core";
import { MatError, MatFormField, MatPrefix } from "@angular/material/form-field";
import { MatIcon } from "@angular/material/icon";
import { MatIconButton } from "@angular/material/button";
import { MatInput } from "@angular/material/input";
import { MatProgressSpinner } from "@angular/material/progress-spinner";
import { MatToolbar } from "@angular/material/toolbar";
import { MatTooltip } from "@angular/material/tooltip";
import { NgObjectPipesModule } from "ngx-pipes";
import { NgTemplateOutlet } from "@angular/common";
import { ExternalFormTableComponent } from "../external-form-table/external-form-table.component";

@Component({
  standalone: true,
  selector: 'app-editable-table',
  templateUrl: './editable-table.component.html',
  styleUrl: './editable-table.component.scss',
  imports: [
    MatError,
    MatFormField,
    MatIcon,
    MatIconButton,
    MatInput,
    MatPaginator,
    MatPrefix,
    MatProgressSpinner,
    MatToolbar,
    MatTooltip,
    MatxErrorsModule,
    MatxInputModule,
    MatxMenuButtonModule,
    MatxTableModule,
    NgObjectPipesModule,
    ReactiveFormsModule,
    FormsModule,
    NgTemplateOutlet
  ],
})
export class EditableTableComponent<T extends { id?: number }> extends ExternalFormTableComponent<T> {

  saveTableRow(table: MatxTableComponent, row: MatxRow) {

    const form = row.form as FormGroup;
    form.markAllAsTouched();
    if (form.invalid) return;

    row.loading = true;
    const item = _merge(row.item, form.value);

    this.service.save(item).pipe(
      finalize(() => row.loading = false),
    ).subscribe(result => {
      row.item = result;
      row.editing = false;
      row.creating = false;
    })
  }

}
