import { delay, firstValueFrom, Observable } from "rxjs";
import { Page } from "../../models/page";
import { ActivatedRoute, Router, RouterLink } from "@angular/router";
import { MatxErrorsModule, MatxInputModule, MatxMenuButtonModule, MatxPromptService } from "matx-core";
import { map, switchMap, tap } from "rxjs/operators";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { debounceFn } from "debounce-decorator-ts";
import { CrudService } from "../../services/crud.service";
import {
  MatxColumn,
  MatxColumnDirective,
  MatxColumnTemplateDirective,
  MatxTableComponent,
  MatxTableModule
} from "matx-table";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AfterViewInit, Component, ContentChildren, Input, ViewChild } from "@angular/core";
import { MatError, MatFormField, MatPrefix } from "@angular/material/form-field";
import { MatIcon } from "@angular/material/icon";
import { MatIconAnchor, MatIconButton } from "@angular/material/button";
import { MatInput } from "@angular/material/input";
import { MatProgressSpinner } from "@angular/material/progress-spinner";
import { MatToolbar } from "@angular/material/toolbar";
import { MatTooltip } from "@angular/material/tooltip";
import { NgObjectPipesModule } from "ngx-pipes";
import { NgTemplateOutlet } from "@angular/common";

@Component({
  standalone: true,
  selector: 'app-external-form-table',
  templateUrl: './external-form-table.component.html',
  styleUrl: './external-form-table.component.scss',
  imports: [
    MatError,
    MatFormField,
    MatIcon,
    MatIconButton,
    MatInput,
    MatPaginator,
    MatPrefix,
    MatProgressSpinner,
    MatToolbar,
    MatTooltip,
    MatxErrorsModule,
    MatxInputModule,
    MatxMenuButtonModule,
    MatxTableModule,
    NgObjectPipesModule,
    ReactiveFormsModule,
    FormsModule,
    NgTemplateOutlet,
    MatIconAnchor,
    RouterLink
  ],
})
export class ExternalFormTableComponent<T extends { id?: number }> implements AfterViewInit {

  dataSource$: Observable<T[]>;

  params: any = {};

  page: Page<T> = {
    size: 20,
    pageNumber: 1
  };

  @Input()
  header = '';

  @Input()
  expand = '';

  @Input()
  service!: CrudService<T>;

  @Input()
  searchFn = (searchParam: string) => `name==*${searchParam}*`

  @ContentChildren(MatxColumn)
  columns!: MatxColumnDirective[];

  @ViewChild('table')
  table!: MatxTableComponent;

  @ViewChild('actionsColumn', {read: MatxColumnTemplateDirective})
  actionsColumn!: MatxColumnTemplateDirective;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    protected promptCtrl: MatxPromptService,
  ) {
    this.dataSource$ = route.queryParams.pipe(
      delay(1),
      switchMap(params => {
        this.params = {...params};
        delete this.params._refresh;
        const page = Number(params['page']);
        const size = Number(params['size']);
        const search = params['search'];
        const _params: {search?: string, page?: number, size?: number, $expand: string} = {
          ...(search && {search: this.searchFn(search)}),
          ...this.extractPage(page, -1),
          ...this.extractSize(size),
          ...(this.expand && {$expand: this.expand})
        };
        this.page.pageNumber = (_params.page ?? 0) + 1;
        return this.service.find(_params).pipe(map(_page => {
          this.page.pageNumber = _page.pageNumber + 1;
          this.page.totalElements = _page.totalElements;
          return _page.content ?? [];
        }));
      }));
  }

  private extractPage(page: number, increase: number) {
    return page && page > 0 && {page: page + increase};
  }

  private extractSize(size: number) {
    return size && size > 0 && size != 20 && {size};
  }

  ngAfterViewInit(): void {
    this.table.columns = [...this.columns, this.actionsColumn]
  }

  navigate(params: any) {
    this.router.navigate([], {relativeTo: this.route, queryParams: params});
  }

  handlePageChange(event: PageEvent) {

    this.navigate({
      search: this.params.search,
      ...this.extractPage(event.pageIndex, 1),
      ...this.extractSize(event.pageSize)
    });
  }

  @debounceFn(500)
  search(search: string) {
    this.navigate({search: search || null});
  }

  refresh() {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {_refresh: new Date().getTime()},
      skipLocationChange: true
    });
  }

  deleteById(id: any) {
    this.promptCtrl.prompt({
      message: 'Are you sure you want to delete this item?',
      actions: [{text: 'No', color: undefined}, {
        text: 'Yes',
        color: 'warn',
        showLoading: true,
        callback: () => firstValueFrom(this.service.deleteById(id).pipe(tap(() => this.refresh())))
      }]
    })
  }

}
