import {Product} from "./product";

export class ShoppingCart {
  id: string;
  lines: ShoppingCartLine[];

  constructor(other?: ShoppingCart) {
    this.id = other?.id ?? 'guest_' + Math.random().toString(36).substring(2, 9);
    this.lines = other?.lines.map(otherLine => new ShoppingCartLine(otherLine)) ?? [];
  }

  get totalItems(): number {
    return this.lines.reduce((total, line) => total + line.quantity, 0);
  }

  get totalPriceBeforeTaxes(): number {
    return this.lines.reduce((total, line) => total + line.linePrice, 0);
  }
}

export class ShoppingCartLine {
  product?: Product;
  quantity: number = 0;

  constructor(other?: ShoppingCartLine) {
    this.product = new Product(other?.product);
    this.quantity = other?.quantity ?? 0;
  }

  get linePrice(): number {
    return (this.product?.calculatedPrice ?? 0) * this.quantity;
  }
}
