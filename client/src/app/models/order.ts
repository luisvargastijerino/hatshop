import { Customer } from "./customer";
import { Shipping } from "./shipping";
import { Tax } from "./tax";
import { Audit } from "./audit";
import { OrderDetail } from "./order-detail";

export interface Order extends Record<string, unknown> {
  id?: number;
  customer?: Customer;
  shipping?: Shipping;
  tax?: Tax;
  totalAmount: number;
  createdOn?: Date;
  shippedOn?: Date;
  status: number;
  comments?: string;
  authCode?: string;
  reference?: string;
  audits?: Audit[];
  orderDetails: OrderDetail[];
}
