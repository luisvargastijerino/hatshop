import { Role } from './role';

export interface User extends Record<string, unknown> {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  roles: Role[];
  accountNonExpired: boolean,
  accountNonLocked: boolean,
  credentialsNonExpired: boolean,
  enabled: boolean
}
