import { Product } from "./product";

export interface OrderDetail extends Record<string, unknown> {
  id?: OrderDetailId;
  product?: Product;
  quantity: number;
  unitCost: number;
}

interface OrderDetailId {
  orderId: number;
  productId: number;
}
