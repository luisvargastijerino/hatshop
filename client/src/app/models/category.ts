import { Department } from './department';

export interface Category extends Record<string, unknown> {
  id?: number;
  name: string;
  description: string;
  department?: Department
}
