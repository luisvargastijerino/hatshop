import { Category } from "./category";
import { Review } from "./review";

export class Product implements Record<string, unknown> {
  id?: number;
  name?: string;
  description?: string;
  price?: number;
  discountedPrice?: number;
  image?: string;
  thumbnail?: string;
  display?: number;
  categories?: Category[];
  reviews?: Review[];

  constructor(other?: Product) {
    this.id = other?.id;
    this.name = other?.name;
    this.description = other?.description;
    this.price = other?.price;
    this.discountedPrice = other?.discountedPrice;
    this.image = other?.image;
    this.thumbnail = other?.thumbnail;
    this.display = other?.display;
    this.categories = other?.categories;
    this.reviews = other?.reviews;
  }

  [x: string]: unknown;

  get calculatedPrice(): number {
    return (this.discountedPrice || this.price) ?? 0;
  }
}
