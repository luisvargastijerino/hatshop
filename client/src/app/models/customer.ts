import { User } from "./user";
import { ShippingRegion } from "./shipping-region";
import { Review } from "./review";
import { Order } from "./order";

export interface Customer extends User {
  shippingRegion?: ShippingRegion;
  creditCard?: string;
  address1?: string;
  address2?: string;
  city?: string;
  region?: string;
  postalCode?: string;
  country?: string;
  dayPhone?: string;
  evePhone?: string;
  mobPhone?: string;
  orders: Order[];
  reviews: Review[];
}
