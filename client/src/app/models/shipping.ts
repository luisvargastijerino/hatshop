import { ShippingRegion } from './shipping-region';

export interface Shipping extends Record<string, unknown> {
  id?: number;
  shippingType: string;
  shippingCost: number;
  shippingRegion: ShippingRegion;
}
