
export interface Tax extends Record<string, unknown> {
  id?: number;
  taxType?: String;
  taxPercentage?: number;
}
