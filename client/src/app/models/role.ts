
export interface Role extends Record<string, unknown> {
  id?: number;

  role: string;
}
