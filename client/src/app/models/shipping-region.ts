import { Shipping } from './shipping';

export interface ShippingRegion extends Record<string, unknown> {
  id: number;
  shippingRegion: string;
  shippings: Shipping[];
}
