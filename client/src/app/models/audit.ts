import { Order } from "./order";

export interface Audit extends Record<string, unknown> {
  id: number;
  order: Order;
  createdOn: Date;
  message: string;
  messageNumber: number;
}
