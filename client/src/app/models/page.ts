
export interface Page<T> {
  pageNumber: number;
  size: number;
  totalElements?: number;
  content?: T[];
}
