import { Category } from './category';

export interface Department extends Record<string, unknown> {
  id?: number;
  name: string;
  description: string;
  categories: Category[]
}
