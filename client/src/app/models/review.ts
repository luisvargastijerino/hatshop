import { Product } from "./product";
import { Customer } from "./customer";

export interface Review extends Record<string, unknown> {
  id: number;
  customer: Customer;
  product: Product;
  review: string;
  rating: number;
  createdOn: Date;
}
