import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class XCsrfTokenInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const xhr = request.method !== 'GET'
      ? request.clone({
        headers: request.headers.set('X-Csrf-Token', sessionStorage.getItem('csrfToken') ?? '')
      })
      : request;
    return next.handle(xhr);
  }
}
