import { TestBed } from '@angular/core/testing';

import { XCsrfTokenInterceptor } from './x-csrf-token.interceptor';

describe('XCsrfTokenInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      XCsrfTokenInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: XCsrfTokenInterceptor = TestBed.inject(XCsrfTokenInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
