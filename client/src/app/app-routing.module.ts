import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ngxPermissionsGuard} from 'ngx-permissions';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
  {
    path: 'products/:productId',
    loadChildren: () => import('./pages/product/product.module').then(m => m.ProductModule)
  },
  {
    path: 'shopping-cart',
    loadChildren: () => import('./pages/shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule)
  },
  {path: 'departments', redirectTo: 'departments/1', pathMatch: 'full'},
  {path: 'departments/:departmentId', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
  {
    path: 'departments/:departmentId/categories/:categoryId',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)},
  {path: 'administration', redirectTo: 'administration/departments', pathMatch: 'full'},
  {
    path: 'administration/departments',
    loadChildren: () => import('./pages/departments/departments.module').then(m => m.DepartmentsModule),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
  {
    path: 'administration/categories',
    loadChildren: () => import('./pages/categories/categories.module').then(m => m.CategoriesModule),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
  {
    path: 'administration/shipping-regions',
    loadChildren: () => import('./pages/shipping-region/shipping-region.module').then(m => m.ShippingRegionModule),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
  {
    path: 'administration/taxes',
    loadComponent: () => import('./pages/taxes/taxes.component').then(m => m.TaxesComponent),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
  {
    path: 'administration/orders',
    loadChildren: () => import('./pages/orders/orders.module').then(m => m.OrdersModule),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
  {
    path: 'administration/products',
    loadChildren: () => import('./pages/products/products.module').then(m => m.ProductsModule),
    canActivate: [ngxPermissionsGuard],
    data: {permissions: {only: ['ADMIN'], redirectTo: 'home'}}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
