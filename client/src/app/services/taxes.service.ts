import { Injectable } from '@angular/core';
import { CrudService } from "./crud.service";
import { Tax } from "../models/tax";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TaxesService extends CrudService<Tax> {

  constructor(http: HttpClient) { super('taxes', http); }
}
