import { TestBed } from '@angular/core/testing';

import { ShippingsService } from './shippings.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('ShippingsService', () => {
  let service: ShippingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ShippingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
