import { TestBed } from '@angular/core/testing';

import { ShippingRegionsService } from './shipping-regions.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NgxPermissionsModule } from "ngx-permissions";

describe('ShippingRegionsService', () => {
  let service: ShippingRegionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgxPermissionsModule,
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(ShippingRegionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
