import {Injectable} from '@angular/core';
import {CrudService} from './crud.service';
import {HttpClient} from '@angular/common/http';
import {Product} from "../models/product";
import {ShoppingCart, ShoppingCartLine} from "../models/shopping-cart";
import {Observable, of, switchMap} from "rxjs";
import {GlobalsService} from "./globals.service";

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService extends CrudService<ShoppingCart> {

  constructor(http: HttpClient, private globalsSvc: GlobalsService) {
    super('shopping-carts', http);
  }

  getGuestCart(): ShoppingCart {
    let guestCartStr = sessionStorage.getItem('guestCart');
    if (guestCartStr) {
      return new ShoppingCart(JSON.parse(guestCartStr));
    }
    const guestCart = new ShoppingCart();
    sessionStorage.setItem('guestCart', JSON.stringify(guestCart));
    return guestCart;
  }

  get shoppingCart$(): Observable<ShoppingCart> {
    return this.globalsSvc.currentUser$.pipe(switchMap(user => {
      let userId = user?.id.toString();
      if (userId) {
        return this.findById(userId);
      } else {
        return of(this.getGuestCart());
      }
    }));
  }


  override save(shoppingCart: ShoppingCart): Observable<ShoppingCart> {
    if (shoppingCart.id.includes("guest")) {
      sessionStorage.setItem('guestCart', JSON.stringify(shoppingCart));
      return of(shoppingCart);
    }
    return super.save(shoppingCart);
  }

  addProductToShoppingCart(shoppingCart: ShoppingCart, product: Product) {
    const shoppingCartLine = shoppingCart.lines.find(line => line.product?.id === product.id);
    if (!shoppingCartLine) {
      shoppingCart.lines.push({
        product,
        quantity: 1
      } as ShoppingCartLine);
    } else {
      shoppingCartLine.quantity++;
    }
    this.save(shoppingCart);
  }
}
