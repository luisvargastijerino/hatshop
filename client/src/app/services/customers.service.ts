import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';
import { HttpClient } from '@angular/common/http';
import { Customer } from "../models/customer";

@Injectable({
  providedIn: 'root'
})
export class CustomersService extends CrudService<Customer> {

  constructor(http: HttpClient) { super('customers', http); }
}
