export const environment = {
  production: false,
  api: '/api/',
  productImagesDir: 'https://raw.githubusercontent.com/Apress/beg-php-postgresql-e-commerce/master/Code%20Download/Images/product_images/',
};
