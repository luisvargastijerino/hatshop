-- Create hatshop tables

-- Create department table
CREATE TABLE departments
(
  id          SERIAL      NOT NULL,
  name        VARCHAR(50) NOT NULL,
  description VARCHAR(1000),
  CONSTRAINT pk_department_id PRIMARY KEY (id)
);

-- Create category table
CREATE TABLE categories
(
  id            SERIAL      NOT NULL,
  department_id INTEGER     NOT NULL,
  name          VARCHAR(50) NOT NULL,
  description   VARCHAR(1000),
  CONSTRAINT pk_category_id PRIMARY KEY (id),
  CONSTRAINT fk_category_to_department_id FOREIGN KEY (department_id)
    REFERENCES departments (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create product table
CREATE TABLE products
(
  id               SERIAL         NOT NULL,
  name             VARCHAR(50)    NOT NULL,
  description      VARCHAR(1000)  NOT NULL,
  price            NUMERIC(10, 2) NOT NULL,
  discounted_price NUMERIC(10, 2) NOT NULL DEFAULT 0.00,
  image            VARCHAR(150),
  thumbnail        VARCHAR(150),
  display          SMALLINT       NOT NULL DEFAULT 0,
  search_vector    VARCHAR(1000),
  CONSTRAINT pk_product PRIMARY KEY (id)
);

-- Create product_category table
CREATE TABLE product_category
(
  product_id  INTEGER NOT NULL,
  category_id INTEGER NOT NULL,
  CONSTRAINT pk_product_to_category_id PRIMARY KEY (product_id, category_id),
  CONSTRAINT fk_product_category_to_product_id FOREIGN KEY (product_id)
    REFERENCES products (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_category_id FOREIGN KEY (category_id)
    REFERENCES categories (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create shopping_cart table
CREATE TABLE shopping_carts
(
  id         CHAR(32)  NOT NULL,
  product_id INTEGER   NOT NULL,
  quantity   INTEGER   NOT NULL,
  buy_now    BOOLEAN   NOT NULL DEFAULT true,
  added_on   TIMESTAMP NOT NULL,
  CONSTRAINT pk_cart_id_product_id PRIMARY KEY (id, product_id),
  CONSTRAINT fk_cart_to_product_id FOREIGN KEY (product_id)
    REFERENCES products (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create shipping_region table
CREATE TABLE shipping_regions
(
  id              SERIAL       NOT NULL,
  shipping_region VARCHAR(100) NOT NULL,
  CONSTRAINT pk_shipping_region_id PRIMARY KEY (id)
);

-- Create shipping table
CREATE TABLE shippings
(
  id                 SERIAL         NOT NULL,
  shipping_type      VARCHAR(100)   NOT NULL,
  shipping_cost      NUMERIC(10, 2) NOT NULL,
  shipping_region_id INTEGER        NOT NULL,
  CONSTRAINT pk_shipping_id PRIMARY KEY (id),
  CONSTRAINT fk_shipping_to_shipping_region_id FOREIGN KEY (shipping_region_id)
    REFERENCES shipping_regions (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create tax table
CREATE TABLE taxes
(
  id             SERIAL         NOT NULL,
  tax_type       VARCHAR(100)   NOT NULL,
  tax_percentage NUMERIC(10, 2) NOT NULL,
  CONSTRAINT pk_tax_id PRIMARY KEY (id)
);

-- Create customers table
CREATE TABLE customers
(
  id                 SERIAL       NOT NULL,
  credit_card        TEXT,
  address1           VARCHAR(100),
  address2           VARCHAR(100),
  city               VARCHAR(100),
  region             VARCHAR(100),
  postal_code        VARCHAR(100),
  country            VARCHAR(100),
  shipping_region_id INTEGER      NOT NULL DEFAULT 1,
  day_phone          VARCHAR(100),
  eve_phone          VARCHAR(100),
  mob_phone          VARCHAR(100),
  CONSTRAINT pk_customer_id PRIMARY KEY (id),
  CONSTRAINT fk_customer_to_shipping_region_id FOREIGN KEY (shipping_region_id)
    REFERENCES shipping_regions (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);
-- CREATE users TABLE
CREATE TABLE users
(
  account_non_expired     BOOLEAN      NOT NULL,
  account_non_locked      BOOLEAN      NOT NULL,
  credentials_non_expired BOOLEAN      NOT NULL,
  enabled                 BOOLEAN      NOT NULL,
  id                      SERIAL       NOT NULL,
  username                VARCHAR(30)  NOT NULL UNIQUE,
  email                   VARCHAR(50)  NOT NULL UNIQUE,
  first_name              VARCHAR(50)  NOT NULL,
  last_name               VARCHAR(50)  NOT NULL,
  password                VARCHAR(100) NOT NULL,
  CONSTRAINT pk_user_id PRIMARY KEY (id),
  CONSTRAINT uk_user_email UNIQUE (email)
);

-- CREATE roles TABLE
CREATE TABLE roles
(
  id   INTEGER NOT NULL,
  role VARCHAR(255),
  CONSTRAINT pk_role_id PRIMARY KEY (id)
);

create table users_roles
(
  roles_id integer not null,
  user_id  integer not null,
  CONSTRAINT pk_users_to_roles_id PRIMARY KEY (roles_id, user_id),
  CONSTRAINT fk_users_roles_to_roles_id FOREIGN KEY (roles_id)
    REFERENCES roles (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_users_roles_to_user_id FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create orders table
CREATE TABLE orders
(
  id           SERIAL         NOT NULL,
  total_amount NUMERIC(10, 2) NOT NULL DEFAULT 0.00,
  created_on   TIMESTAMP      NOT NULL,
  shipped_on   TIMESTAMP,
  status       INTEGER        NOT NULL DEFAULT 0,
  comments     VARCHAR(255),
  customer_id  INTEGER,
  auth_code    VARCHAR(50),
  reference    VARCHAR(50),
  shipping_id  INTEGER,
  tax_id       INTEGER,
  CONSTRAINT pk_order_id PRIMARY KEY (id),
  CONSTRAINT fk_orders_to_customer_id FOREIGN KEY (customer_id)
    REFERENCES customers (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_orders_to_shipping_id FOREIGN KEY (shipping_id)
    REFERENCES shippings (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_orders_to_tax_id FOREIGN KEY (tax_id)
    REFERENCES taxes (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create order_detail table
CREATE TABLE order_details
(
  order_id     INTEGER        NOT NULL,
  product_id   INTEGER        NOT NULL,
  product_name VARCHAR(50)    NOT NULL,
  quantity     INTEGER        NOT NULL,
  unit_cost    NUMERIC(10, 2) NOT NULL,
  CONSTRAINT pk_order_id_product_id PRIMARY KEY (order_id, product_id),
  CONSTRAINT fk_order_id FOREIGN KEY (order_id)
    REFERENCES orders (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create audit table
CREATE TABLE audits
(
  id             SERIAL    NOT NULL,
  order_id       INTEGER   NOT NULL,
  created_on     TIMESTAMP NOT NULL,
  message        TEXT      NOT NULL,
  message_number INTEGER   NOT NULL,
  CONSTRAINT pk_audit_id PRIMARY KEY (id),
  CONSTRAINT fk_audit_to_order_id FOREIGN KEY (order_id)
    REFERENCES orders (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);

-- Create review table
CREATE TABLE reviews
(
  id          SERIAL    NOT NULL,
  customer_id INTEGER   NOT NULL,
  product_id  INTEGER   NOT NULL,
  review      TEXT      NOT NULL,
  rating      SMALLINT  NOT NULL,
  created_on  TIMESTAMP NOT NULL,
  CONSTRAINT pk_review_id PRIMARY KEY (id),
  CONSTRAINT fk_review_to_customer_id FOREIGN KEY (customer_id)
    REFERENCES customers (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_review_to_product_id FOREIGN KEY (product_id)
    REFERENCES products (id)
    ON UPDATE RESTRICT ON DELETE RESTRICT
);
